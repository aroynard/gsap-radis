const radisSprite = document.getElementById("radisSprite");
const RADIS_SPRITE_STEP_X = 360;
const RADIS_SPRITE_MAX_X = 3240;
const RADIS_SPRITE_STEP_Y = 346;
const RADIS_SPRITE_MAX_Y = 3460;
let lastPosition = window.scrollY;
let bgdYPosition = 0;
let bgdXPosition = 0;
let rotateCount = 0;
let rotationCycle = false;

setInterval(() => {
  adjustBackgroundPosition(-1);
  radisSprite.style.backgroundPosition = `${bgdXPosition}px ${bgdYPosition}px`;
  radisSprite.style.transform = `scale(0.7) rotate(${rotateCount * 0.1}deg)`;
}, 10);

ScrollTrigger.create({
  trigger: "#radisSprite",
  start: "top bottom",
  end: "bottom top",
  // markers: true,
  onUpdate: () => {
    requestAnimationFrame(() => {
      if (window.scrollY > lastPosition) {
        rotateCount--;
        adjustBackgroundPosition(-1);
      } else if (window.scrollY < lastPosition) {
        rotateCount++;
        adjustBackgroundPosition(1);
      }

      radisSprite.style.backgroundPosition = `${bgdXPosition}px ${bgdYPosition}px`;
      radisSprite.style.transform = `scale(0.7) rotate(${rotateCount * 0.1}deg)`;
      lastPosition = window.scrollY;
    });
  },
});

function adjustBackgroundPosition(direction) {
  rotationCycle ? rotateCount++ : rotateCount--;
  toggleRotationCycle();

  if (direction < 0) {
    bgdXPosition -= RADIS_SPRITE_STEP_X;

    if (bgdXPosition <= -RADIS_SPRITE_MAX_X) {
      bgdXPosition = 0;
      bgdYPosition -= RADIS_SPRITE_STEP_Y;

      if (bgdYPosition <= -RADIS_SPRITE_MAX_Y) bgdYPosition = 0;
    }
  } else {
    bgdXPosition += RADIS_SPRITE_STEP_X;

    if (bgdXPosition >= RADIS_SPRITE_STEP_X) {
      bgdXPosition = -RADIS_SPRITE_MAX_X;
      bgdYPosition += RADIS_SPRITE_STEP_Y;

      if (bgdYPosition >= RADIS_SPRITE_STEP_Y) bgdYPosition = -RADIS_SPRITE_MAX_Y + RADIS_SPRITE_STEP_Y;
    }
  }
};

function toggleRotationCycle() {
  if (Math.abs(rotateCount) === 180) {
    rotationCycle = !rotationCycle;
  }
}
